package s1;

class A extends B {
}

class B {
    private String param;
    E e;
    D d;

    public void x() {
        System.out.println("metoda x");
    }

    public void y() {
        System.out.println("metdoa y");
    }
}


class U {
    public U() {
    }

    public U(B b) {
        b.x();
        b.y();
    }
}

class E {
}

class C {
    B b = new B();
}

class D {

}