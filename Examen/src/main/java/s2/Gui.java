package s2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;


public class Gui extends JFrame {
    private JButton button;
    private JTextArea textArea;
    private static final Random RANDOM = new Random();
    private int i;

    public Gui() {
        this.setTitle("Random number generator");
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setLayout(null);
        this.setBounds(100, 100, 300, 400);

        textArea = new JTextArea();
        textArea.setEditable(false);
        JScrollPane scrollPane = new JScrollPane(textArea);
        scrollPane.setBounds(5, 5, 280, 300);
        this.add(scrollPane);

        button = new JButton("Generate");
        button.setBounds(5, 330, 280, 25);
        button.addActionListener(new Generate());
        this.add(button);

        this.setVisible(true);
    }

    private class Generate implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            i = RANDOM.nextInt();
            textArea.append(i + "\n");
        }
    }

    public static void main(String[] args) {
        new Gui();
    }
}
